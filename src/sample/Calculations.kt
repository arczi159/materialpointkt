package sample

object Calculations {

    fun positionGeometricCenter(point: Array<Point2D?>): Point2D {
        val point2D = Point2D(0.0, 0.0)
        for (pkt in point) {
            point2D.x = point2D.x + pkt!!.x
            point2D.y = point2D.y + pkt.y
        }
        point2D.x = point2D.x / 2
        point2D.y = point2D.y / 2
        return point2D
    }

    fun positionCenterOfMass(materialPoint: Array<MaterialPoint2D?>): Point2D {
        val materialPoint2D = MaterialPoint2D(1.0, 1.0, 0.0)
        for (pkt in materialPoint) {
            materialPoint2D.x = pkt!!.x * pkt.mass
            materialPoint2D.y = pkt.y * pkt.mass
            materialPoint2D.mass = materialPoint2D.mass + pkt.mass
        }
        materialPoint2D.x = materialPoint2D.x / materialPoint2D.mass
        materialPoint2D.y = materialPoint2D.y / materialPoint2D.mass
        return materialPoint2D
    }


}