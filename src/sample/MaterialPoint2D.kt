package sample

class MaterialPoint2D(x: Double, y: Double, var mass: Double) : Point2D(x, y) {

    override fun toString(): String {
        return "x = $x y = $y masa = $mass"
    }
}