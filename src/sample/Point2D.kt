package sample

open class Point2D(var x: Double, var y: Double) {

    override fun toString(): String {
        return "x = " + x +
                ", y = " + y
    }
}

